package database;

import org.testng.Assert;

import java.sql.*;

public class DataBaseTest {
    public static void main(String[] args) throws SQLException {

        String url = "jdbc:oracle:thin:@batch4db1.cup7q3kvh5as.us-east-2.rds.amazonaws.com:1521/ORCL";
        String username = "melda";
        String password = "melda123!";
        String query = "SELECT first_name ,last_name from employees\n" +
                "where manager_id=(SELECT manager_id from employees \n" +
                "where first_name='Payam')";

        Connection connection = DriverManager.getConnection(url, username, password);
        System.out.println("Database connection is successful");

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

        while (resultSet.next()) {

            String firstName = resultSet.getString("FIRST_NAME"); // FIRST_NAME is the column name
            String lastName = resultSet.getString("LAST_NAME"); // LAST_NAME is the column name
            System.out.println(firstName + "                          " + lastName);

            // Validating the data from the table
            if (firstName.equals("Neena") && lastName.equals("Kochhar")) {
                String actualName = firstName;
                Assert.assertEquals(actualName, "Neena");
                System.out.println("Actual name: " + firstName + "  " + lastName);
                break;
            }
        }
    }

}
