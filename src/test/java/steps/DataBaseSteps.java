package steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.testng.Assert;
import utils.DBUtil;

import java.util.ArrayList;
import java.util.List;

public class DataBaseSteps {
    List<List<Object>> classQuery= new ArrayList<>();

    @Given("User is able to connect to database")
    public void userIsAbleToConnectToDatabase() {
        DBUtil.createDBConnection();
    }

    @When("User send the {string} to database")
    public void userSendTheToDatabase(String query){
     classQuery=DBUtil.getQueryResultList(query);
    }

    @And("Employees expected first and last names")
    public void employeesExpectedFirstAndLastNames(DataTable dataTable) {
        List<List<String>> expectedResult=dataTable.asLists();
        for (int i = 0; i < expectedResult.size(); i++) {
            for (int j = 0; j < 2 ; j++) {
                Assert.assertEquals(classQuery.get(i).get(j),expectedResult.get(i).get(j));
                System.out.println(expectedResult.get(i).get(j));
                System.out.println(classQuery.get(i).get(j));
            }
        }
    }
}